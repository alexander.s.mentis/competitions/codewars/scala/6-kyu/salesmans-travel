object Travel {

  def travel(r: String, zipcode: String): String = {
  
    def parse(s: String): (String, String, String) = {
      // return (addr, street_city, zip) 
      val (first, rest) = s.splitAt(s.indexOf(' '))
      val (middle, last) = rest.splitAt(rest.length-8)
      (first, middle.trim, last)
    }
    
    val (addr, st_city, zip) = r.split(",")
                                .map(parse(_))
                                .filter(_._3 == zipcode)
                                .unzip3
                           
    zipcode + ":" + st_city.mkString(",") + "/" + addr.mkString(",")
  }
}